package com.example.crosphere

import java.util.concurrent.CountDownLatch
import android.view.MotionEvent
import android.view.View

abstract class Player {
    var crosphereDelegate: CrosphereDelegate? = null
    abstract val name: String
    abstract fun move(state: State): Move
    override fun toString(): String {
        return "type=${this::class.simpleName}"
    }

    private var strongStonesOn = false

    fun areStrongStonesOn(): Boolean{
        return strongStonesOn
    }

    fun setStrongStonesOn(value: Boolean){
        strongStonesOn = value
    }
    fun Boolean.toInt() = if (this) 1 else 0
}

class Human() : Player() {
    override val name: String = "Your"
    override fun move(state: State): Move {
        val latch = CountDownLatch(1)
        var x = -1
        var y = -1
        val view: View = crosphereDelegate!!.getMyView()

        view.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                y = crosphereDelegate!!.getCol(event.x)
                x = crosphereDelegate!!.getRow(event.y)
                v.performClick()
                latch.countDown()
            }
            true
        }
        latch.await()
        return (crosphereDelegate!!.areStrongStonesOn().toInt() to (x to y))
    }
}

class TestPlayer(private val moves: List<Move>) : Player() {
    override val name: String = "TestPlayer's"
    private var movesCount = 0
    override fun move(state: State): Move {
        movesCount++
        return moves[movesCount - 1]
    }
}
