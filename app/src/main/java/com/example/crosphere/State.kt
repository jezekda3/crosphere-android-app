package com.example.crosphere
import java.util.*

fun strBoard(board: Array<IntArray>): String {
    var strBoard = "\n  "
    for (i in board.indices) {
        strBoard += "$i "
    }
    strBoard += "\n"
    for ((i, row) in board.withIndex()) {
        strBoard += "$i "
        for (cell in row) {
            strBoard += cell
            strBoard += " "
        }
        strBoard += "\n"
    }
    return strBoard
}

class State(
    private var _boardSize: Int = 8,
    private var _numOwnStones: MutableList<Int>? = null,
    private var _board: Array<IntArray>? = null,
    private var _movesCount: Int = 0
) {
    private val boardSizeLock = Any()
    private val numOwnStonesLock = Any()
    private val boardLock = Any()
    private val movesCountLock = Any()
    private val gameFinishedLock = Any()
    private val wonLock = Any()

    var boardSize: Int
        @Synchronized get() = synchronized(boardSizeLock) { _boardSize }
        @Synchronized set(value) { synchronized(boardSizeLock) { _boardSize = value } }

    var numOwnStones: MutableList<Int>?
        @Synchronized get() = synchronized(numOwnStonesLock) { _numOwnStones }
        @Synchronized set(value) { synchronized(numOwnStonesLock) { _numOwnStones = value } }

    var board: Array<IntArray>?
        @Synchronized get() = synchronized(boardLock) { _board }
        @Synchronized set(value) { synchronized(boardLock) { _board = value } }

    var movesCount: Int
        @Synchronized get() = synchronized(movesCountLock) { _movesCount }
        @Synchronized set(value) { synchronized(movesCountLock) { _movesCount = value } }

    init {
        if (_board == null) {
            _board = Array(_boardSize) { IntArray(_boardSize) }
        }
        if (_numOwnStones == null) {
            _numOwnStones = mutableListOf(5, 5)
        }
    }

    var gameFinished = false
        @Synchronized get() = synchronized(gameFinishedLock) { field }
        @Synchronized set(value) { synchronized(gameFinishedLock) { field = value } }

    var won = booleanArrayOf(false, false)
        @Synchronized get() = synchronized(wonLock) { field }
        @Synchronized set(value) { synchronized(wonLock) { field = value } }


    override fun hashCode(): Int {
        return Arrays.deepHashCode(board)
    }

    override fun toString(): String {
        return """
            ${strBoard(board!!)}
            Moves Played: $movesCount
            On turn: Player ${movesCount % 2 + 1}
            Number of Own Stones: $numOwnStones
            Game Finished: $gameFinished
            Won: ${won.contentToString()}
            """.trimIndent()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is State) return false
        return board!!.contentDeepEquals(other.board) &&
                movesCount == other.movesCount &&
                numOwnStones == other.numOwnStones
    }

    fun deepCopy(): State {
        val newBoard = Array(boardSize) { board!![it].copyOf() }
        val newState = State(8, numOwnStones?.toMutableList(), newBoard, movesCount)
        newState.gameFinished = gameFinished
        newState.won = won.copyOf()
        return newState
    }
}