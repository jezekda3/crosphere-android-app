package com.example.crosphere

import android.util.Log
import kotlin.math.exp

fun sigmoid(x: Double): Double {
    return 1 / (1 + exp(-x))
}

fun bfs(state: State): Array<Array<IntArray>> {
    val opened = arrayOf(arrayListOf(), arrayListOf<Position>())
    val closed = arrayOf(arrayListOf(), arrayListOf<Position>())
    val distances = arrayOf(
        Array(state.boardSize + 1) { IntArray(state.boardSize) { 100 } },
        Array(state.boardSize) { IntArray(state.boardSize + 1) { 100 } }
    )

    for (i in 0 until state.boardSize) {
        opened[0].add((state.boardSize to i))
        opened[1].add((i to state.boardSize))
        distances[0][state.boardSize][i] = 0
        distances[1][i][state.boardSize] = 0
    }

    for (i in 0 until 2) {
        while (opened[i].isNotEmpty()) {
            val current = opened[i].removeAt(0)
            for (neighbour in neighbours(current, state.board!!)) {
                if (neighbour !in opened[i] && neighbour !in closed[i] && state.board!![neighbour.first][neighbour.second] != 2 + (1 - i)) {
                    if (state.board!![neighbour.first][neighbour.second] == 0) {
                        opened[i].add(neighbour)
                        distances[i][neighbour.first][neighbour.second] = distances[i][current.first][current.second] + 1
                    } else {
                        opened[i].add(0, neighbour)
                        distances[i][neighbour.first][neighbour.second] = distances[i][current.first][current.second]
                    }
                }
            }
            closed[i].add(current)
        }
    }
    return distances
}

class MCTSheur(
    explorationWeight: Double = 0.5,
    timeForMove: Long? = null,
    rolloutsPerMove: Int? = null,
    private val stoneWeight: Double = 0.5,
    override var name: String = "AI's"
) : MCTSsim(explorationWeight, timeForMove, rolloutsPerMove) {

    override fun evaluate(state: State): Double {
        val distances = bfs(state)
        val pl1Distances = distances[0].first()
        val pl2Distances = distances[1].map { it[0] }
        val pl1Dist = pl1Distances.minOrNull() ?: 0
        val pl2Dist = pl2Distances.minOrNull() ?: 0
        if (pl1Dist == 0 && pl2Dist != 0) {
            return 1.0
        }
        if (pl2Dist == 0 && pl1Dist != 0) {
            return 0.0
        }
        val pl1Stones = state.numOwnStones!![0]
        val pl2Stones = state.numOwnStones!![1]
        return sigmoid(pl2Dist - pl1Dist + stoneWeight * (pl1Stones - pl2Stones))
    }
}
