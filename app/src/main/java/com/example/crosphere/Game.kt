package com.example.crosphere

import java.text.SimpleDateFormat
import java.util.*
import android.os.Build
import android.util.Log

fun deviceInfo(): String {
    return "system=${Build.VERSION.SDK_INT}, node=${Build.MODEL}"
}

fun tag(name: String, content: String): String {
    return "[$name \"$content\"]\n"
}

fun convertStone(stoneType: Int, movesCount: Int): Int {
    if (stoneType == 1) {
        return if (movesCount % 2 == 1) 3 else 2
    }
    return 1
}

fun neighbours(pos: Position, board: Array<IntArray>): List<Position> {
    val (posX, posY) = pos
    val neighbours = mutableListOf<Position>()
    if (posX > 0) {
        neighbours.add((posX - 1 to posY))
    }
    if (posX < board.size - 1) {
        neighbours.add((posX + 1 to posY))
    }
    if (posY > 0) {
        neighbours.add((posX to posY - 1))
    }
    if (posY < board[0].size - 1) {
        neighbours.add((posX to posY + 1))
    }
    return neighbours
}


class Game(
    var player1: Player? = null,
    var player2: Player? = null,
    var state: State? = null,
    val onScreen: Boolean = true,
) {
    private var opened: MutableList<MutableList<Position>> = mutableListOf(mutableListOf(), mutableListOf())
    private var closed: MutableList<MutableList<Position>> = mutableListOf(mutableListOf(), mutableListOf())
    private var history: MutableList<Move> = mutableListOf()
    var crosphereDelegate: CrosphereDelegate? = null
    private var lastMove: Move? = null

    init {
        if (state == null) {
            state = State()
        }
        if (player1 == null) {
            player1 = Human()
        }
        if (player2 == null) {
            player2 = Human()
        }
        opened[0] = mutableListOf()
        opened[1] = mutableListOf()
        for (i in 0 until state!!.boardSize) {
            if (state!!.board!![0][i] == 1 || state!!.board!![0][i] == 2) {
                opened[0].add((0 to i))
            }
            if (state!!.board!![i][0] == 1 || state!!.board!![i][0] == 3) {
                opened[1].add((i to 0))
            }
        }
        bfs(intArrayOf(0, 0))
    }

    override fun toString(): String {
        val gameStr = StringBuilder()
        gameStr.append(tag("Player_1", player1.toString()))
        gameStr.append(tag("Player_2", player2.toString()))
        gameStr.append(tag("Result", getResult().toString()))
        gameStr.append(tag("Datetime", SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(Date())))
        gameStr.append(tag("Device", deviceInfo()))
        gameStr.append("\n")
        history.forEachIndexed { i, move ->
            gameStr.append("${i + 1}. $move\n")
        }
        return gameStr.toString() + "\n\n"
    }

    fun getLastMove(): Move?{
        return lastMove
    }

    fun stoneAt(row: Int, col: Int): Int {
        return this.state!!.board!![row][col]
    }

    fun playerOnTurn(): Player? {
        if (this.state!!.movesCount % 2 == 0){
            return this.player1
        }
        return this.player2
    }

    private fun checkConnection(pos: Position): IntArray {
        val numInserted = intArrayOf(0, 0)
        for (i in 0..1) {
            if (state!!.board!![pos.first][pos.second] == 1 || state!!.board!![pos.first][pos.second] == 2 + i) {
                var posIndex = pos.first
                if (i == 1) {
                    posIndex = pos.second
                }
                if (posIndex == 0) {
                    opened[i].add(pos)
                    numInserted[i]++
                }
                if (!opened[i].contains(pos)) {
                    for (neighbour in state!!.board?.let { neighbours(pos, it) }!!) {
                        if (neighbour in closed[i] || neighbour in opened[i]) {
                            opened[i].add(pos)
                            numInserted[i]++
                            break
                        }
                    }
                }
            }
        }
        return numInserted
    }

    private fun bfs(numInserted: IntArray): IntArray {
        for (i in 0..1) {
            while (opened[i].isNotEmpty()) {
                val current = opened[i].removeAt(0)
                var currentIndex = current.first
                if (i == 1) {
                    currentIndex = current.second
                }
                if (currentIndex == state!!.boardSize - 1) {
                    state!!.gameFinished = true
                    state!!.won[i] = true
                }
                for (neighbour in state!!.board?.let { neighbours(current, it) }!!) {
                    if (neighbour !in opened[i] && neighbour !in closed[i] &&
                        (state!!.board!![neighbour.first][neighbour.second] == 1 ||
                                state!!.board!![neighbour.first][neighbour.second] == 2 + i)
                    ) {
                        opened[i].add(neighbour)
                        numInserted[i]++
                    }
                }
                closed[i].add(current)
            }
        }
        return numInserted
    }

    fun isValid(move: Move, modify: Boolean = true): Boolean {
        val (stoneType, pos) = move

        // Check indexes
        val (posX, posY) = pos
        val boardSize = state!!.boardSize
        if (posX < 0 || posX >= boardSize || posY < 0 || posY >= boardSize || stoneType < 0 || stoneType > 1) {
            return false
        }

        // Check if the player has enough own stones
        if (stoneType == 1) {
            if (state!!.numOwnStones!![state!!.movesCount % 2] <= 0) {
                return false
            }
        }

        // Check if the square is free
        if (state!!.board!![pos.first][pos.second] != 0) {
            return false
        }

        state!!.board!![pos.first][pos.second] = convertStone(stoneType, state!!.movesCount)
        val numInserted = bfs(checkConnection(pos))
        state!!.board!![pos.first][pos.second] = 0

        // Check if the move does not result in a simultaneous
        // win for both sides
        val bothWin = state!!.won[0] && state!!.won[1]
        if (!modify || bothWin) {
            state!!.won[0] = false
            state!!.won[1] = false
            state!!.gameFinished = false
            if (numInserted[0] > 0) {
                closed[0] = closed[0].take(closed[0].size - numInserted[0]).toMutableList()
            }
            if (numInserted[1] > 0) {
                closed[1] = closed[1].take(closed[1].size - numInserted[1]).toMutableList()
            }
        }

        return !bothWin
    }

    private fun getResult(): Double {
        return when {
            state!!.won[0] && !state!!.won[1] -> 1.0
            !state!!.won[0] && state!!.won[1] -> 0.0
            !state!!.won[0] && !state!!.won[1] -> 0.5
            else -> -1.0
        }
    }

    private fun isDraw(): Boolean {
        if (state!!.numOwnStones!![state!!.movesCount % 2] >= 1) {
            return false
        }
        for (i in 0 until state!!.boardSize) {
            for (j in 0 until state!!.boardSize) {
                val move = (0 to (i to j))
                if (isValid(move, false)) {
                    return false
                }
            }
        }
        state!!.gameFinished = true
        return true
    }

    fun play(): Double {
        while (!state!!.gameFinished && !isDraw()) {
            val playerOnTurn = if (state!!.movesCount % 2 == 0) player1 else player2
            if (playerOnTurn is Human){
                crosphereDelegate?.updateStrongStoneView() }
            crosphereDelegate?.updateTextViewAnnouncement()
            crosphereDelegate?.updateAnnouncementBackground(0)
            while (true) {
                val move = playerOnTurn!!.move(state!!)
                if (isValid(move)) {
                    lastMove = move
                    val (stoneType, pos) = move
                    if (stoneType == 1) {
                        state!!.numOwnStones!![state!!.movesCount % 2] -= 1
                    }
                    state!!.board!![pos.first][pos.second] = convertStone(stoneType, state!!.movesCount)
                    history.add(move)
                    if (onScreen) { crosphereDelegate?.updateView() }
                    break
                }
            }
            state!!.movesCount++
        }
        state!!.movesCount--
        crosphereDelegate?.updateTextViewAnnouncement()
        crosphereDelegate?.updateAnnouncementBackground(0)
        state!!.movesCount++
        return getResult()
    }
}