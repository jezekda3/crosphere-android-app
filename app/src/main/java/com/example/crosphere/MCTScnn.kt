package com.example.crosphere

import android.content.Context
import android.util.Log
import com.example.crosphere.ml.Model5
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.nio.ByteBuffer
import java.nio.ByteOrder

fun preprocessState(state: State): Array<Array<Array<FloatArray>>> {
    val distances = bfs(state)
    val distances_0 = Array(8) { Array(8) { 0 } }
    val distances_1 = Array(8) { Array(8) { 0 } }
    for (i in 0 until 8) {
        for (j in 0 until 8) {
            distances_0[i][j] = distances[0][i][j]
            distances_1[i][j] = distances[1][i][j]
        }
    }
    //distances[0] = distances[0].sliceArray(0 until state.boardSize)
    val pl1Dists = distances_0[0]
    val pl2Dists = distances_1.map { it[0] }
    val pl1Dist = pl1Dists.minOrNull() ?: 0
    val pl2Dist = pl2Dists.minOrNull() ?: 0
    val emptyFields = Array(state.boardSize) { i ->
        IntArray(state.boardSize) { j -> if (state.board!![i][j] == 0) 1 else 0 }
    }
    val commonStones = Array(state.boardSize) { i ->
        IntArray(state.boardSize) { j -> if (state.board!![i][j] == 1) 1 else 0 }
    }
    val ownStones1 = Array(state.boardSize) { i ->
        IntArray(state.boardSize) { j -> if (state.board!![i][j] == 2) 1 else 0 }
    }
    val ownStones2 = Array(state.boardSize) { i ->
        IntArray(state.boardSize) { j -> if (state.board!![i][j] == 3) 1 else 0 }
    }
    val intArray = arrayOf(
        emptyFields,
        commonStones,
        ownStones1,
        ownStones2,
        distances[0],
        distances[1],
        Array(state.boardSize) { IntArray(state.boardSize) { pl1Dist } },
        Array(state.boardSize) { IntArray(state.boardSize) { pl2Dist } },
        Array(state.boardSize) { IntArray(state.boardSize) { state.movesCount % 2 } },
        Array(state.boardSize) { IntArray(state.boardSize) { state.numOwnStones!![0] } },
        Array(state.boardSize) { IntArray(state.boardSize) { state.numOwnStones!![1] } }
    )

    val floatArray: Array<Array<Array<FloatArray>>> = Array(1) { _ ->
        Array(11) { i ->
            Array(8) { j ->
                FloatArray(8) { k ->
                    intArray[i][j][k].toFloat()
                }
            }
        }
    }

    return floatArray
}


class MCTScnn(
    explorationWeight: Double = 0.5,
    timeForMove: Long? = null,
    rolloutsPerMove: Int? = null,
    private val context: Context,
    override var name: String = "AI's"
) : MCTSsim(explorationWeight, timeForMove, rolloutsPerMove) {

    private val model = Model5.newInstance(context)

    override fun evaluate(state: State): Double {
        val block = preprocessState(state)
        val pl1Dist = block[0][6][0][0]
        val pl2Dist = block[0][7][0][0]

        //Log.d(TAG, "pl1Dist: ${pl1Dist.toInt()}")
        //Log.d(TAG, "pl2Dist: ${pl2Dist.toInt()}")

        if (pl1Dist.toInt() == 0 && pl2Dist.toInt() != 0) {
            //Log.d(TAG, "first would win")
            return 1.0
        }
        if (pl2Dist.toInt() == 0 && pl1Dist.toInt() != 0) {
            //Log.d(TAG, "second would win")
            return 0.0
        }

        val originalShape = intArrayOf(1, 11, 8, 8)
        val byteBuffer = ByteBuffer.allocateDirect(originalShape.reduce { acc, i -> acc * i } * Float.SIZE_BYTES)
        byteBuffer.order(ByteOrder.nativeOrder()) // Set the byte order

        // Copy the data to the ByteBuffer without flattening
        for (i in 0 until originalShape[0]) { // Batch size
            for (j in 0 until originalShape[1]) { // Channels
                for (k in 0 until originalShape[2]) { // Height
                    for (l in 0 until originalShape[3]) { // Width
                        val value = block[i][j][k][l]
                        byteBuffer.putFloat(value)
                    }
                }
            }
        }

        byteBuffer.rewind() // Reset position to the beginning

        val inputFeature0 = TensorBuffer.createFixedSize(intArrayOf(1, 11, 8, 8), DataType.FLOAT32)
        inputFeature0.loadBuffer(byteBuffer)

        //val model = Model5.newInstance(context)
        val outputs = model.process(inputFeature0)
        val outputFeature0 = outputs.outputFeature0AsTensorBuffer
        //model.close()
        val output = sigmoid(outputFeature0.getFloatValue(0).toDouble())
        //Log.d(TAG, "output: $output")
        return output
    }
}