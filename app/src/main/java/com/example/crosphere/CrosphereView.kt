package com.example.crosphere

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.math.min
import androidx.core.content.ContextCompat

class CrosphereView : View {
    private val cellSide: Float = 110f
    private val margin: Float = 8f
    private var originX: Float = 0f
    private var originY: Float = 0f
    private var boardSize: Int = 8
    private val borderWidth: Float = 15f
    private val paint = Paint()
    var crosphereDelegate: CrosphereDelegate? = null

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {}

    enum class CellType(val colorResourceId: Int) {
        EMPTY(R.color.empty_field),
        WEAK(R.color.weak_stone),
        STRONG_FIRST(R.color.strong_stone_1),
        STRONG_SECOND(R.color.strong_stone_2);
    }

    fun getCol(x: Float): Int {
        return ((x - originX  - margin / 2) / (cellSide + margin)).toInt()
    }

    fun getRow(y: Float): Int {
        return ((y - originY  - margin / 2) / (cellSide + margin)).toInt()
    }

    override fun onDraw(canvas: Canvas) {
        originX = (width - boardSize * cellSide - (boardSize + 1) * margin - borderWidth * 2) / 2
        originY = (height - boardSize * cellSide - (boardSize + 1) * margin - borderWidth * 2) / 2

        val lastMove = crosphereDelegate?.getLastMove()
        if (lastMove != null){
            paint.color = ContextCompat.getColor(context, R.color.last_move)
            canvas.drawRect(
                originX + borderWidth + margin + lastMove.second.second * (cellSide + margin) - margin,
                originY + borderWidth + margin + lastMove.second.first * (cellSide + margin) - margin,
                originX + borderWidth + margin + lastMove.second.second * (cellSide + margin) + cellSide + margin,
                originY + borderWidth + margin + lastMove.second.first * (cellSide + margin) + cellSide + margin, paint
            )
        }

        for (i in 0..<boardSize){
            for (j in 0..<boardSize) {
                paint.color = ContextCompat.getColor(context, CellType.entries[crosphereDelegate?.stoneAt(j, i)!!].colorResourceId)
                canvas.drawRect(
                    originX + borderWidth + margin + i * (cellSide + margin),
                    originY + borderWidth + margin + j * (cellSide + margin),
                    originX + borderWidth + margin + i * (cellSide + margin) + cellSide,
                    originY + borderWidth + margin + j * (cellSide + margin) + cellSide, paint
                )
            }
        }

        paint.color = ContextCompat.getColor(context, CellType.entries[3].colorResourceId)
        canvas.drawRect(
            originX,
            originY + borderWidth + margin,
            originX + borderWidth,
            originY + borderWidth + boardSize * (cellSide + margin), paint
        )
        canvas.drawRect(
            originX + margin + borderWidth + boardSize * (cellSide + margin),
            originY + borderWidth + margin,
            originX + margin + 2 * borderWidth + boardSize * (cellSide + margin),
            originY + borderWidth + boardSize * (cellSide + margin), paint
        )

        paint.color = ContextCompat.getColor(context, CellType.entries[2].colorResourceId)
        canvas.drawRect(
            originX + borderWidth + margin,
            originY,
            originX + borderWidth + boardSize * (cellSide + margin),
            originY + borderWidth, paint
        )
        canvas.drawRect(
            originX + borderWidth + margin,
            originY + borderWidth + margin + boardSize * (cellSide + margin),
            originX + borderWidth + boardSize * (cellSide + margin),
            originY + 2* borderWidth + margin + boardSize * (cellSide + margin), paint
        )
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?: return false
        when (event.action){
            MotionEvent.ACTION_DOWN -> {
                Log.d(TAG, "down at (${getRow(event.y)}, ${getCol(event.x)})")
            }
            MotionEvent.ACTION_MOVE -> {
                Log.d(TAG, "move at (${getRow(event.y)}, ${getCol(event.x)})")
            }
            MotionEvent.ACTION_UP -> {
                Log.d(TAG, "up at (${getRow(event.y)}, ${getCol(event.x)})")
                performClick()
            }
        }
        return true
    }

    override fun performClick(): Boolean {
        super.performClick()
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val smaller = min(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(smaller, smaller)
    }
}