package com.example.crosphere

import android.view.View


interface CrosphereDelegate {
    fun getLastMove(): Move?
    fun updateAnnouncementBackground(percentage: Int)
    fun updateTextViewAnnouncement()
    fun updateStrongStoneView()
    fun getCol(x: Float): Int
    fun getRow(y: Float): Int
    fun stoneAt(row: Int, col: Int): Int?
    fun playerOnTurn(): Player?
    fun updateView()
    fun getMyView():View
    fun areStrongStonesOn(): Boolean
    fun setStrongStonesOn(value: Boolean)
}