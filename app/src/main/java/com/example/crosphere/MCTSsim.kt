package com.example.crosphere

import android.util.Log
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executors
import kotlin.random.Random
import kotlin.collections.HashMap
import kotlin.reflect.KFunction2

//val limitedDispatcher = Executors.newFixedThreadPool(8).asCoroutineDispatcher()


fun findMoves(state: State): List<Move> {
    val boardSize = state.board?.size
    val game = Game(player1 = null, player2 = null, state = state, onScreen = false)
    if (game.state!!.gameFinished) {
        return emptyList()
    }
    val moves = mutableListOf<Move>()
    var stoneTypes = listOf(0)
    if (state.numOwnStones!![state.movesCount % 2] >= 1) {
        stoneTypes = listOf(0, 1)
    }
    for (stoneType in stoneTypes) {
        for (i in 0 until boardSize!!) {
            for (j in 0 until boardSize) {
                val move = (stoneType to (i to j))
                if (game.isValid(move, false)) {
                    moves.add(move)
                }
            }
        }
    }
    return moves
}

fun extractMove(parent: State, child: State): Move {
    for (i in parent.board!!.indices) {
        for (j in parent.board!![i].indices) {
            if (parent.board!![i][j] != child.board!![i][j]) {
                return ((if (child.board!![i][j] > 1) 1 else 0) to (i to j))
            }
        }
    }
    throw IllegalArgumentException("Both states are equal!\n${parent}\n${child}")
}

class RandomPlayer : Player() {
    override val name: String = "RandomPlayer's"
    override fun move(state: State): Move {
        return (
            Random.nextInt(2) to
            (Random.nextInt(state.board!!.size) to Random.nextInt(state.board!!.size))
        )
    }
}

class DefaultMovesDict : HashMap<State, MutableList<Move>>() {
    fun getOrCreate(key: State): MutableList<Move> {
        return getOrPut(key) { findMoves(key).toMutableList() }
    }
}


open class MCTSsim(
    private var _explorationWeight: Double = 1.0,
    private var _timeForMove: Long? = null,
    private var _rolloutsPerMove: Int? = null,
    override var name: String = "AI's"
) : Player() {
    private val explorationWeightLock = Any()
    private val timeForMoveLock = Any()
    private val rolloutsPerMoveLock = Any()
    private val rewardsLock = Any()
    private val visitsLock = Any()
    private val childrenLock = Any()
    private val unexpandedMovesLock = Any()

    private var explorationWeight: Double
        @Synchronized get() = synchronized(explorationWeightLock) { _explorationWeight }
        @Synchronized set(value) { synchronized(explorationWeightLock) { _explorationWeight = value } }

    private var timeForMove: Long?
        @Synchronized get() = synchronized(timeForMoveLock) { _timeForMove }
        @Synchronized set(value) { synchronized(timeForMoveLock) { _timeForMove = value } }

    private var rolloutsPerMove: Int?
        @Synchronized get() = synchronized(rolloutsPerMoveLock) { _rolloutsPerMove }
        @Synchronized set(value) { synchronized(rolloutsPerMoveLock) { _rolloutsPerMove = value } }

    private val rewards = HashMap<State, Double>()
    private val visits = HashMap<State, Int>()
    private val children = HashMap<State, MutableList<State>>()
    private val unexpandedMoves = DefaultMovesDict()
    private var startTime: Long = 0

    @Synchronized
    fun <T> MutableList<T>.popRandom(): T? {
        synchronized(unexpandedMovesLock){
            if (isEmpty()) return null
            val index = Random.nextInt(size)
            return removeAt(index)
        }
    }

    private fun getRewards(state: State): Double {
        synchronized(rewardsLock) {
            return rewards.getOrDefault(state, 0.0)
        }
    }

    private fun setRewards(state: State, value: Double) {
        synchronized(rewardsLock) {
            rewards[state] = value
        }
    }

    private fun getVisits(state: State): Int {
        synchronized(visitsLock) {
            return visits.getOrDefault(state, 0)
        }
    }

    private fun setVisits(state: State, value: Int) {
        synchronized(visitsLock) {
            visits[state] = value
        }
    }

    private fun getChildren(state: State): MutableList<State> {
        synchronized(childrenLock) {
            return children.getOrPut(state) { mutableListOf() }
        }
    }

    private fun getUnexpandedMoves(state: State): MutableList<Move> {
        synchronized(unexpandedMovesLock) {
            return unexpandedMoves.getOrCreate(state)
        }
    }

    private fun relativeReward(child: State): Double {
        return kotlin.math.abs(((child.movesCount + 1) % 2) * getVisits(child) - getRewards(child))
    }

    private fun ucbScore(node: State, child: State): Double {
        val w = relativeReward(child)
        val n = getVisits(child)
        val c = explorationWeight
        val t = getVisits(node)
        return w / n + c * kotlin.math.sqrt(kotlin.math.log(t.toDouble(), 2.0) / n)
    }

    private fun bestScore(node: State, child: State): Double {
        return relativeReward(child) / getVisits(child)
    }

    private fun choose(node: State, scoreMetric: KFunction2<State, State, Double>): State {
        var bestScore = 0.0
        var bestChild = getChildren(node)[0]
        synchronized(childrenLock){
            for (child in getChildren(node)) {
                val score = scoreMetric(node, child)
                if (score > bestScore) {
                    bestScore = score
                    bestChild = child
                }
            }
        }
        return bestChild
    }

    private fun select(inputNode: State): MutableList<State> {
        var node = inputNode
        val path = mutableListOf(node)
        while (true) {
            val movesList = getUnexpandedMoves(node)
            if (movesList.isNotEmpty()) {
                return path
            }
            if (getChildren(node).isEmpty()) {
                return path
            }
            node = choose(node, ::ucbScore)
            path.add(node)
        }
    }

    private fun createChild(node: State, move: Move): State {
        val (newStone, newPos) = move
        val newChild = node.deepCopy()
        newChild.board!![newPos.first][newPos.second] = convertStone(newStone, node.movesCount)
        newChild.movesCount++
        if (newStone == 1) {
            newChild.numOwnStones!![node.movesCount % 2]--
        }
        getChildren(node).add(newChild)
        return newChild
    }

    private fun expand(path: MutableList<State>) {
        val node = path.last()
        val newMove = getUnexpandedMoves(node).popRandom() ?: return
        path.add(createChild(node, newMove))
    }

    protected open fun evaluate(state: State): Double {
        return Game(
            player1 = RandomPlayer(),
            player2 = RandomPlayer(),
            state = state.deepCopy(),
            onScreen = false,
        ).play()
    }

    private fun backpropagation(reward: Double, path: MutableList<State>) {
        while (path.isNotEmpty()) {
            val node = path.removeAt(0)
            setVisits(node, getVisits(node) + 1)
            setRewards(node, getRewards(node) + reward)
        }
    }
    private fun rollout(node: State) {
        val path = select(node)
        expand(path)
        val reward = evaluate(path.last())
        backpropagation(reward, path)
    }

    private fun timeIsUp(): Boolean {
        if (timeForMove == null) {
            return false
        }
        return System.currentTimeMillis() - startTime >= timeForMove!!
    }

    private fun rolloutsAreUp(rolloutsCompleted: Int): Boolean {
        if (rolloutsPerMove == null) {
            return false
        }
        return rolloutsCompleted >= rolloutsPerMove!!
    }

    //@OptIn(DelicateCoroutinesApi::class)
    override fun move(state: State): Move {
        val node = state.deepCopy()
        var rolloutsCompleted = 0
        //val jobs = mutableListOf<Deferred<Unit>>()
        startTime = System.currentTimeMillis()
        while (true) {
            //val job = GlobalScope.async(limitedDispatcher) {
                rollout(node)
            //}
            //jobs.add(job)
            rolloutsCompleted++
            crosphereDelegate!!.updateAnnouncementBackground(((rolloutsCompleted.toDouble() / rolloutsPerMove!!.toDouble())*100).toInt())
            if (timeIsUp() || rolloutsAreUp(rolloutsCompleted)) {
                break
            }
        }
        /*runBlocking {
            jobs.awaitAll()
        }*/
        val chosenState = choose(node, ::bestScore)
        return extractMove(state, chosenState)
    }
}
