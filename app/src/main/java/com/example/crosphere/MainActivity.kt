package com.example.crosphere

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.activity.ComponentActivity
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import android.app.AlertDialog
import android.view.LayoutInflater

typealias Position = Pair<Int, Int>
typealias Move = Pair<Int, Position>
const val TAG = "my_output"
class MainActivity() : ComponentActivity(), CrosphereDelegate{
    private var game = Game()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<CrosphereView>(R.id.crosphere_view).crosphereDelegate = this
        updateView()
        showAboutPopup(true)
    }

    fun onAboutButtonClicked(view: View) {
        showAboutPopup(false)
    }

    fun onStartGameButtonClicked(view: View) {
        showStartGamePopup()
    }


    private fun showAboutPopup(showChooseOpponentAfter: Boolean) {
        val dialogBuilder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.about, null)

        dialogBuilder.setView(view)
        val alertDialog = dialogBuilder.create()

        view.findViewById<Button>(R.id.okButton).setOnClickListener {
            alertDialog.dismiss()
            if (showChooseOpponentAfter){
                showChooseOpponentPopup()
            }
        }

        alertDialog.show()
    }

    private fun showStartGamePopup() {
        val dialogBuilder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.start_game, null)

        dialogBuilder.setView(view)
        val alertDialog = dialogBuilder.create()

        view.findViewById<Button>(R.id.okButton).setOnClickListener {
            showChooseOpponentPopup()
            alertDialog.dismiss()
        }

        view.findViewById<Button>(R.id.cancelButton).setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    private fun showChooseOpponentPopup() {
        val dialogBuilder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.choose_opponent, null)

        dialogBuilder.setView(view)
        val alertDialog = dialogBuilder.create()

        view.findViewById<Button>(R.id.sim).setOnClickListener {
            alertDialog.dismiss()
            showChooseStrengthPopup(0)
        }

        view.findViewById<Button>(R.id.heur).setOnClickListener {
            alertDialog.dismiss()
            showChooseStrengthPopup(1)
        }

        view.findViewById<Button>(R.id.CNN).setOnClickListener {
            alertDialog.dismiss()
            showChooseStrengthPopup(2)
        }

        alertDialog.show()
    }

    private fun createPlayer(playerType: Int, rollouts: Int): Player{
        return if (playerType == 0){
            MCTSsim(_explorationWeight = 0.3, _rolloutsPerMove = rollouts, name = "Your opponent")
        } else if (playerType == 1){
            MCTSheur(explorationWeight = 0.3, rolloutsPerMove = rollouts, stoneWeight = 0.4, name = "Your opponent")
        } else{
            MCTScnn(explorationWeight = 0.3, rolloutsPerMove = rollouts, context = this, name = "Your opponent")
        }
    }

    private fun showChooseStrengthPopup(playerType: Int) {
        val dialogBuilder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.choose_strength, null)

        dialogBuilder.setView(view)
        val alertDialog = dialogBuilder.create()

        view.findViewById<Button>(R.id.roll_1).setOnClickListener {
            alertDialog.dismiss()
            showChooseColorPopup(createPlayer(playerType, 125))
        }

        view.findViewById<Button>(R.id.roll_2).setOnClickListener {
            alertDialog.dismiss()
            showChooseColorPopup(createPlayer(playerType, 250))
        }

        view.findViewById<Button>(R.id.roll_3).setOnClickListener {
            alertDialog.dismiss()
            showChooseColorPopup(createPlayer(playerType, 500))
        }

        alertDialog.show()
    }


    private fun showChooseColorPopup(opponent: Player) {
        val dialogBuilder = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.choose_side, null)

        dialogBuilder.setView(view)
        val alertDialog = dialogBuilder.create()

        view.findViewById<View>(R.id.strong_stone_first).setOnClickListener {
            alertDialog.dismiss()
            game = Game()
            game.player1 = Human()
            game.player2 = opponent
            playGame()
        }

        view.findViewById<View>(R.id.strong_stone_second).setOnClickListener {
            alertDialog.dismiss()
            game = Game()
            game.player1 = opponent
            game.player2 = Human()
            playGame()
        }

        alertDialog.show()
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun playGame(){
        updateView()
        updateStrongStoneView()
        game.player1!!.crosphereDelegate = this
        game.player2!!.crosphereDelegate = this
        game.crosphereDelegate = this

        GlobalScope.launch(Dispatchers.IO) {
            try {
                findViewById<View>(R.id.strong_stone_view).setOnClickListener{
                    if (playerOnTurn() is Human && !game.state!!.gameFinished){
                        setStrongStonesOn(!areStrongStonesOn())
                        updateStrongStoneView()
                    }
                }
                game.play()
            }catch(e: Exception){
                Log.d(TAG, Log.getStackTraceString(e))
            }
        }
    }

    override fun updateStrongStoneView(){
        var clr = R.drawable.weak_stone
        if (areStrongStonesOn()) {
            clr = R.drawable.strong_stone_first
            if (playerOnTurn() == game.player2) {
                clr = R.drawable.strong_stone_second
            }
        }
        val view = findViewById<View>(R.id.strong_stone_view)
        view.setBackgroundResource(clr)
        view.invalidate()
    }

    private fun winningPlayer(): Player?{
        if (game.state!!.won[0] && !game.state!!.won[1]){
            return game.player1!!
        }
        if (game.state!!.won[1] && !game.state!!.won[0]){
            return game.player2!!
        }
        return null
    }

    override fun updateTextViewAnnouncement(){
        val view = findViewById<TextView>(R.id.textViewAnnouncement)
        var playerName = playerOnTurn()?.name ?: ""
        if (!game.state!!.gameFinished) {
            val textResourceId =
                if (playerOnTurn() is Human) R.string.human_turn_placeholder else R.string.turn_placeholder
            view.text = getString(textResourceId, playerName)
        }
        else{
            if (winningPlayer() == null){
                view.text = "It is a draw!"
            }
            playerName = winningPlayer()!!.name
            if (winningPlayer() is Human){ playerName = "You" }
            view.text = getString(R.string.game_finished, playerName)
        }
    }

    override fun getLastMove(): Move? {
        return game.getLastMove()
    }

    override fun updateAnnouncementBackground(percentage: Int) {
        runOnUiThread {
            val progressBar = findViewById<ProgressBar>(R.id.progressBar3)
            progressBar.progress = (percentage)
        }
    }

    override fun getCol(x: Float): Int {
        return findViewById<CrosphereView>(R.id.crosphere_view).getCol(x)
    }

    override fun getRow(y: Float): Int {
        return findViewById<CrosphereView>(R.id.crosphere_view).getRow(y)
    }

    override fun stoneAt(row: Int, col: Int): Int {
        return game.stoneAt(row, col)
    }

    override fun playerOnTurn(): Player? {
        return game.playerOnTurn()
    }

    override fun getMyView(): View {
        return findViewById<CrosphereView>(R.id.crosphere_view)
    }

    override fun areStrongStonesOn(): Boolean {
        return playerOnTurn()!!.areStrongStonesOn()
    }

    override fun setStrongStonesOn(value: Boolean) {
        return playerOnTurn()!!.setStrongStonesOn(value)
    }

    override fun updateView(){
        findViewById<TextView>(R.id.textViewStrong1).text = getString(R.string.num_own_stones_placeholder, game.state?.numOwnStones?.get(0))
        findViewById<TextView>(R.id.textViewStrong2).text = getString(R.string.num_own_stones_placeholder, game.state?.numOwnStones?.get(1))
        findViewById<CrosphereView>(R.id.crosphere_view).invalidate()
    }
}
